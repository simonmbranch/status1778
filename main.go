package main

import (
	"encoding/json"
	"flag"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"
)

var cacheFlag = flag.Bool("cached", false, "use cached files")

const authKey string = "9LveFPK4Kybur8xZM7SZKKF2cDtYTzNGJz7a974Vr2HfXHhvKjj8ODC5XZ8siv5M"

func check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func readTba(address string) []byte {
	var text []byte
	var err error
	var request *http.Request
	var response *http.Response

	client := http.Client{Timeout: time.Second * 5}
	request, err = http.NewRequest(http.MethodGet,
		"https://www.thebluealliance.com/api/v3/"+address, nil)
	check(err)

	request.Header.Set("X-TBA-Auth-Key", authKey)

	response, err = client.Do(request)
	check(err)
	text, err = ioutil.ReadAll(response.Body)
	response.Body.Close()
	check(err)
	return text
}

type Match struct {
	Number    int    `json:"match_number"`
	Winner    string `json:"winning_alliance"`
	Blue      Alliance
	Red       Alliance
	CompLevel string
}

type Alliance struct {
	Score int
	Teams []int
}

func (a *Alliance) UnmarshalJSON(data []byte) error {
	// Extract team number from "frcXXXX"-style tags.
	var raw struct {
		Teams []string `json:"team_keys"`
		Score int
	}
	if err := json.Unmarshal(data, &raw); err != nil {
		return err
	}
	for _, key := range raw.Teams {
		after, _ := strings.CutPrefix(key, "frc")
		num, _ := strconv.Atoi(after)
		a.Teams = append(a.Teams, num)
	}
	a.Score = raw.Score
	return nil
}

func (m *Match) UnmarshalJSON(data []byte) error {
	var raw struct {
		Winner    string `json:"winning_alliance"`
		Alliances map[string]Alliance
		Number    int    `json:"match_number"`
		CompLevel string `json:"comp_level"`
	}
	if err := json.Unmarshal(data, &raw); err != nil {
		return err
	}
	m.Number = raw.Number
	m.Winner = raw.Winner
	m.Red = raw.Alliances["red"]
	m.Blue = raw.Alliances["blue"]
	m.CompLevel = raw.CompLevel
	return nil
}

func matchesFromJson(text []byte) []Match {
	var matches []Match
	check(json.Unmarshal(text, &matches))
	// Keep only qualifier matches (comp_level = qm)
	{
		n := 0
		for _, m := range matches {
			if m.CompLevel == "qm" {
				matches[n] = m
				n++
			}
		}
		matches = matches[:n]
	}
	// Sort by match number.
	sort.Slice(matches, func(i, j int) bool {
		return matches[i].Number < matches[j].Number
	})
	return matches
}

func main() {
	flag.Parse()
	var text []byte = readTba("event/2024wasno/matches")
	var matches []Match = matchesFromJson(text)
	file, err := os.Create("out.html")
	check(err)
	templ, err := template.ParseFiles("templ.html")
	check(err)
	check(templ.Execute(file, matches))
}
